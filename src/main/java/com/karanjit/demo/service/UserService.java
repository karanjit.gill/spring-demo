package com.karanjit.demo.service;

import com.karanjit.demo.model.User;
import com.karanjit.demo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepo;

    public void addUser (User user) {
        this.userRepo.insert(user);
    }

    public List<User> getAllUsers () {
        return this.userRepo.findAll();
    }
}
