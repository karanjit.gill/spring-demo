# spring-demo

A basic spring boot server.

Info:

* src/main/resources/application.properties
    * mongodb connection url - mongodb://localhost:27017/springDemo

    * Server Port - 8080

* POST JSON format - 

```
{
    "firstName" : "fname",
    "lastName" : "lname"
}
```

| Methods     | Endpoints   | Purpose                   |
|:-----------:|:-----------:|:-------------------------:|
| GET         | /           | To Get all User documents |
| POST        | /           | To Insert a User document |
